module.exports = {
  port: 3000,

  files: {
    urls: {
      base: {
        dev: 'http://at.local/',
        test: 'http://test.at.govt.nz/',
        preprod: 'http://pp.at.govt.nz/',
        prod: 'http://at.govt.nz/'
      },
      header: [
        {
          title: 'Bus Train Ferry',
          url: 'bus-train-ferry',
          topNav: true,
          icons: [
            'bus',
            'train',
            'ferry'
          ]
        },
        {
          title: 'Cycling & Walking',
          url: 'cycling-walking',
          topNav: true,
          icons: [
            'bike',
            'walk'
          ]
        },
        {
          title: 'Driving & Parking',
          url: 'driving-parking',
          topNav: true,
          icons: [
            'car',
            'park'
          ]
        },
        {
          title: 'Projects & Roadworks',
          url: 'projects-roadworks',
          topNav: true,
          icons: [
            'tools'
          ]
        },
        {
          title: 'About us',
          url: 'myat/',
          topNav: true,
          icons: [
            'about'
          ]
        },
        {
          title: 'MyAT',
          url: 'about-us/',
          topNav: false
        }
      ],
      footer: {
        contactUs: {
          title: 'Contact Us',
          url: 'about-us/contact-us/'
        },
        careers: {
          title: 'Careers',
          url: 'about-us/careers/'
        },
        twitter: {
          title: 'Twitter',
          url: 'https://twitter.com/AklTransport/'
        },
        youtube: {
          title: 'YouTube',
          url: 'https://www.youtube.com/user/aucklandtransport/'
        },
        govtNz: {
          title: 'New Zealand Government',
          url: 'https://www.govt.nz/'
        },
        copyright: {
          title: 'Copyright',
          url: 'about-us/about-this-site/terms-conditions/copyright-statement/'
        },
        nav: [
          {
            title: 'Terms of use',
            url: 'about-us/about-this-site/terms-conditions/'
          },
          {
            title: 'Accessibility',
            url: 'about-us/about-this-site/accessibility/'
          },
          {
            title: 'Privacy',
            url: 'about-us/about-this-site/privacy-policy/'
          },
          {
            title: 'About This Site',
            url: 'about-us/about-this-site/'
          }
          // ,
          // {
          //   title: 'Copyright',
          //   url: 'about-us/about-this-site/terms-conditions/copyright-statement/'
          // }
        ]
      }
    },
      // header: {
      //   busTrainFerry: {
      //     title: 'Bus Train Ferry',
      //     url: 'bus-train-ferry/'
      //   },
      //   cyclingWalking: {
      //     title: 'Cycling & Walking',
      //     url: 'cycling-walking/'
      //   },
      //   drivingParking: {
      //     title: 'Driving & Parking',
      //     url: 'driving-parking/'
      //   },
      //   projectsRoadworks: {
      //     title: 'Projects & Roadworks',
      //     url: 'projects-roadworks/'
      //   },
      //   aboutUs: {
      //     title: 'About us',
      //     url: 'about-us/'
      //   }
      // }
    // js: {
    //   // Use uncompressed versions of 3rd-pary libraries.
    //   // They will be compressed in production.
    //   // Any libraries added to /vendor must be added here.
    //   // If you remove a library you must remove it here too.
    //   vendor: [
    //     'vendor/momentjs/moment.js',
    //     'vendor/jquery/dist/jquery.js',
    //     'vendor/lodash/dist/lodash.js',
    //     'vendor/angular/angular.js',
    //     'vendor/angular-mocks/angular-mocks.js',
    //     'vendor/angular-ui-router/release/angular-ui-router.js',
    //     'vendor/angular-loading-bar/src/loading-bar.js'
    //   ],

    //   app: [
    //     'app/app.js',
    //     'app/**/*.js',
    //     '!app/**/*-spec.js',
    //     '!app/**/*-scenario.js',
    //   ],

    //   buildDest: 'build/js'
    // },

    sass: {
      main: [
           'src/scss/*.scss'
        // 'app/**/*.scss',
        // '!app/assets/*.scss',
        // '!app/assets/**/*.scss',
        // 'app/common/**/*.scss',
        // 'app/components/**/*.scss',
        // 'vendor/angular-loading-bar/src/loading-bar.scss'
      ],

      buildDest: 'build/css'
    },

    html: {
      index: 'src/index.html',
      header: 'src/html/header.html',
      sidenav: 'src/html/sidenav.html',
      footer: 'src/html/footer.html',
      elements: 'src/html/*.html',
      tpls: {
        all: 'src/**/*-template.html'
      },

      buildDest: {
        base:'build',
        staticHTML:'build/html/static',
        angular:'build/html/angular'
      }
    },

    img: {
      src: 'src/assets/images/**/*',
      buildDest: 'build/img'
    },

    fonts: {
      src: 'src/fonts/*',
      buildDest: 'build/fonts'
    },

    // test: {
    //   e2e: 'app/**/*-scenario.js',

    //   unit: [
    //     'build/js/vendor.js',
    //     'build/js/templates.js',
    //     'build/js/app.js',
    //     'app/**/*-spec.js'
    //   ]
    // }
  }
}