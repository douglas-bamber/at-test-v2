at-html-components
===================

Stores the html and css for the header and footer for the at.govt.nz website.

## Usage:
1. include the directives located in /build/components/ in your angular project

```js
gulp.task('htmlComponents', function () {
	return gulp.src('vendor/at-test-v2/build/components/**')
	.pipe(gulp.dest('app/components'));
});
```

2. include the sprites located in /build/img

```js
gulp.task('imgsComponents', function () {
	return gulp.src('vendor/at-test-v2/build/img/*')
	.pipe(gulp.dest("app/assets/images"));
});
```

3. That's it -- you're done!

#### via bower:
```
$ bower install https://Goudla@bitbucket.org/Goudla/at-test-v2.git --save
```