var gulp = require('gulp'),
	clean = require('gulp-clean'),
	inject = require("gulp-inject"),
	rename = require('gulp-rename'),
	connect = require('connect'),
	mustache = require("gulp-mustache"),
	jshint = require('gulp-jshint'),
	sass = require('gulp-ruby-sass'),
	minifyCSS = require('gulp-minify-css'),
	html2js = require('gulp-html2js'),
	concat = require('gulp-concat'),
	ngmin = require('gulp-ngmin'),
	uglify = require('gulp-uglify'),
	htmlmin = require('gulp-htmlmin'),
	imagemin = require('gulp-imagemin'),
	cache = require('gulp-cache'),
	livereload = require('gulp-livereload'),
	wrap = require('gulp-wrap'),
	svgmin = require('gulp-svgmin'),
	svgSprite = require("gulp-svg-sprites"),
	filter    = require('gulp-filter'),
	raster = require('gulp-raster');
	runSequence = require('run-sequence'),
	iconfont = require('gulp-iconfont'),
	iconfontCss = require('gulp-iconfont-css'),
	files = require('./build.config.js').files;

var productionDir = '_public', // production output directory (default: _public)
	port = require('./build.config.js').port;

// Process Sass files into main.css.
gulp.task('css', function () {
	//return gulp.src('./app/assets/scss/main.scss')
	return gulp.src('./src/scss/main.scss')
		.pipe(sass())
//		.pipe(concat('vendor/angular-loading-bar/build/loading-bar.css'))
		.pipe(rename('main.css'))
		.pipe(gulp.dest(files.sass.buildDest));
});

// Sass files to build dir
gulp.task('scss', function () {
	//return gulp.src('./app/assets/scss/main.scss')
	return gulp.src('./src/scss/layout/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('build/css'));
});

// Bower Components into build dir
gulp.task('components', function () {
	return gulp.src('./src/components/**/*')
		.pipe(gulp.dest('build/components'));
});

gulp.task('mustacheHeader', function () {
	return gulp.src("./src/templates/header.mustache")
		.pipe(mustache({
			title: "Auckland Transport",
			baseUrl: files.urls.base.dev,
			nav: files.urls.header,
			angular: true
		}))
		.pipe(gulp.dest("./src/html"));
});

gulp.task('mustacheFooter', function () {
	return gulp.src("./src/templates/footer.mustache")
		.pipe(mustache({
			title: "Auckland Transport",
			baseUrl: files.urls.base.dev,
			links: files.urls.footer,
			nav: files.urls.footer.nav,
			year: new Date().getFullYear()
		}))
		.pipe(gulp.dest("./src/html"));
});

gulp.task('mustacheSidenav', function () {
	return gulp.src("./src/templates/sidenav.mustache")
		.pipe(mustache({
			baseUrl: files.urls.base.dev,
			nav: files.urls.header,
		}))
		.pipe(gulp.dest("./src/html"));
});

// Angular Header template
gulp.task('headerTemplate', function () {
	return gulp.src("./src/components/header-directive/*.html")
		.pipe(inject(gulp.src([files.html.header]), {
			starttag: '<!-- inject:header:html -->',
			transform: function (filePath, file) {
				// return file contents as string
				return file.contents.toString('utf8')
			}
		}))
		.pipe(gulp.dest("./build/components/header-directive/"));
});

// Angular Header CSS
gulp.task('headerScss', function () {
	return gulp.src("./src/scss/layout/header.scss")
		.pipe(sass())
		.pipe(rename('header-directive.scss'))
		.pipe(gulp.dest("./build/components/header-directive/"));
});

// Angular Footer template
gulp.task('footerTemplate', function () {
	return gulp.src("./src/components/footer-directive/*.html")
		.pipe(inject(gulp.src([files.html.footer]), {
			starttag: '<!-- inject:footer:html -->',
			transform: function (filePath, file) {
				// return file contents as string
				return file.contents.toString('utf8')
			}
		}))
		.pipe(gulp.dest("./build/components/footer-directive/"));
});

// Angular Footer CSS
gulp.task('footerScss', function () {
	return gulp.src("./src/scss/layout/footer.scss")
		.pipe(sass())
		.pipe(rename('footer-directive.scss'))
		.pipe(gulp.dest("./build/components/footer-directive/"));
});





// HTML files into build dir.
gulp.task('html', function () {
	return gulp.src(files.html.index)
		.pipe(inject(gulp.src([files.html.header]), {
			starttag: '<!-- inject:header:html -->',
			transform: function (filePath, file) {
				// return file contents as string
				return file.contents.toString('utf8')
			}
		}))
		.pipe(inject(gulp.src([files.html.footer]), {
			starttag: '<!-- inject:footer:html -->',
			transform: function (filePath, file) {
				// return file contents as string
				return file.contents.toString('utf8')
			}
		}))
		.pipe(inject(gulp.src([files.html.sidenav]), {
			starttag: '<!-- inject:sidenav:html -->',
			transform: function (filePath, file) {
				// return file contents as string
				return file.contents.toString('utf8')
			}
		}))
		.pipe(gulp.dest(files.html.buildDest.base));
});

//
gulp.task('elementsStatic', function () {
	return gulp.src(files.html.elements)
		.pipe(gulp.dest(files.html.buildDest.staticHTML));
});

//
gulp.task('elementsAngular', function () {
	return gulp.src(files.html.elements)
		.pipe(gulp.dest(files.html.buildDest.angular));
});

// Process images.
gulp.task('img', function () {
	return gulp.src(files.img.src)
		.pipe(cache(imagemin({
			optimizationLevel: 5,
			progressive: true,
			interlaced: true})))
		.pipe(gulp.dest(files.img.buildDest));
});

// Clean Processed SVGS.
gulp.task('clean-svgs', function () {
	return gulp.src('src/svg/after/*.svg', {read: false})
		.pipe(clean());
});

gulp.task('svgmin', ['clean-svgs'], function() {
	return gulp.src('./src/svg/before/*.svg')
		.pipe(svgmin())
		.pipe(gulp.dest('./src/svg/after'));
});

var config = {
	cssFile: "scss/_sprite.scss",
	svgPath: "../img/sprite.svg",
	pngPath: "../img/sprite.png",
	templates: {
	css: require("fs").readFileSync("./templates/sprite.css", "utf-8")
	}
};

gulp.task('sprites', function () {
	return gulp.src('./src/svg/after/*.svg')
//		.pipe(svgSprite())
		// .pipe(svgSprite({
		// 	cssFile: "scss/_sprite.scss"
		// }))
		.pipe(svgSprite(config))
		.pipe(gulp.dest('./src/img/sprites')) // Write the sprite-sheet + CSS + Preview
		.pipe(filter('**/*.svg')) // Filter out everything except the SVG file
		//.pipe(svg2png()) // Create a PNG
		.pipe(raster())
		.pipe(rename({extname: '.png'}))
		.pipe(gulp.dest('./src/img/sprites'));
		//.pipe(gulp.dest('./src/svg/sprites'));
});

// .pipe(svgSprite({
//             cssFile: "scss/_sprite.scss"
//         }))

// assets into build dir.
gulp.task('assets', function () {
	return gulp.src('./src/img/sprites/svg/*')
		.pipe(gulp.dest('./build/img'));
});

// gulp.task('iconcss', function () {
// 	return gulp.src('./src/svg/sprites/css/*.css')
// 		.pipe(rename('icons.css'))
// 		.pipe(gulp.dest('build/css/'));
// });

// Process fonts.
gulp.task('fonts', function () {
	return gulp.src(files.fonts.src)
		.pipe(gulp.dest(files.fonts.buildDest));
});



gulp.task('svgminFont', function() {
	return gulp.src('./src/iconfont/before/**/*.svg')
		//.pipe(svgmin())
		.pipe(gulp.dest('./src/iconfont/after'));
});

var iconfont = require('gulp-iconfont');

// gulp.task('Iconfont', function(){
// 	return gulp.src(['./src/svg/after/*.svg'])
// 	.pipe(iconfont({
// 		fontName: 'atfont', // required
// 		appendCodepoints: true // recommended option
// 	}))
// 	.on('codepoints', function(codepoints, options) {
// 	// CSS templating, e.g.
// 	console.log(codepoints, options);
// 	})
// 	.pipe(gulp.dest('build/iconfonts/'));
// });

// var gulp = require('gulp');
// var iconfont = require('gulp-iconfont');
// var consolidate = require('gulp-consolidate');

// gulp.task('Iconfont', function(){
// 	gulp.src(['./src/svg/after/*.svg'])
// 		.pipe(iconfont({ fontName: 'atfont' }))
// 		.on('codepoints', function(codepoints, options) {
// 			gulp.src('templates/myfont.css')
// 			.pipe(consolidate('lodash', {
// 				glyphs: codepoints,
// 				fontName: 'myfont',
// 				fontPath: '../fonts/',
// 				className: 's'
// 			}))
// 			.pipe(gulp.dest('www/css/'));
// 		})
// 		.pipe(gulp.dest('www/fonts/'));
// });

// var fontName = 'atIcons';

// gulp.task('iconfont', function(){
// 	gulp.src(['./src/iconfont/after/*.svg'])
// 		.pipe(iconfontCss({
// 			fontName: fontName,
// 			//path: 'node_modules/gulp-iconfont-css/templates/_icons.scss',
// 			path: 'node_modules/gulp-iconfont-css/templates/_icons.css',
// 			//targetPath: '../../css/_icons.scss',
// 			targetPath: '../css/icons.css',
// 			fontPath: '../../fonts/icons/'
// 		}))
// 		.pipe(iconfont({
// 			fontName: fontName
// 		 }))
// 		.pipe(gulp.dest('build/assets/fonts/icons/'));
// });

// Compile CSS for production.
gulp.task('compile:css', function () {
	return gulp.src('build/**/*.css')
		.pipe(minifyCSS({keepSpecialComments: 0}))
		.pipe(gulp.dest(productionDir));
});

// Compile JS for production.
gulp.task('compile:js', function () {
	return gulp.src('build/**/*.js')
		.pipe(uglify())
		.pipe(gulp.dest(productionDir));
});

// Compile HTML for production.
gulp.task('compile:html', function () {
	return gulp.src('build/**/*.htm*')
		.pipe(htmlmin({collapseWhitespace: true}))
		.pipe(gulp.dest(productionDir));
});

// Prepare images for production.
gulp.task('compile:img', function () {
	return gulp.src('build/img/**')
		.pipe(gulp.dest(productionDir+'/img'));
});

// Prepare images for production.
gulp.task('compile:fonts', function () {
	return gulp.src('build/fonts/*')
		.pipe(gulp.dest(productionDir+'/fonts'));
});

// Update/install webdriver.
//gulp.task('webdriver:update', webdriverUpdate);

// Run webdriver standalone server indefinitely.
// Usually not required.
//gulp.task('webdriver:standalone', ['webdriver:update'], webdriverStandalone);

// Run unit tests using karma.
// gulp.task('karma', function () {
// 	return gulp.src(files.test.unit)
// 		.pipe(karma({
// 			configFile: 'karma.conf.js',
// 			action: 'run'
// 		}))
// 		.on('error', function(e) {throw e});
// });

// Run unit tests using karma and watch for changes.
// gulp.task('karma:watch', function () {
// 	return gulp.src(files.test.unit)
// 		.pipe(karma({
// 			configFile: 'karma.conf.js',
// 			action: 'watch'
// 		}))
// 		.on('error', function(e) {throw e});
// });

// Run e2e tests using protractor.
// Make sure server task is running.
// gulp.task('protractor', ['webdriver:update'], function() {
// 	return gulp.src(files.test.e2e)
// 		.pipe(protractor({
// 			configFile: 'protractor.conf.js',
// 		}))
// 		.on('error', function(e) {throw e});
// });

// Run e2e tests using protractor and watch for changes.
// Make sure server task is running.
// gulp.task('protractor:watch', ['protractor'], function () {
// 	gulp.watch(['build/**/*', files.test.e2e], ['protractor']);
// });

//gulp.task('test', ['karma', 'protractor']);

// Clean build directory.
gulpClean('build');

// Clean production directory.
gulpClean(productionDir);

// Clean build and production directories.
gulp.task('clean', function (callback) {
	runSequence(['clean:build', 'clean:'+productionDir], callback);
});

// Build files for local development.
gulp.task('build', function (callback) {
	runSequence(
	'clean:build',
//	'clean:src/svg/after/**',
	'mustacheHeader',
	'mustacheFooter',
	'mustacheSidenav',
	'svgmin',
	'svgminFont',
	'sprites',
	'components',
	[
	//'js:vendor',
	//	'js:app',
	//	'js:templates-all',
	//	'iconcss',
	//	'iconfont',
		'headerTemplate',
		'headerScss',
		'footerTemplate',
		'footerScss',
		'assets',
		'css',
		'scss',
		'html',
		'elementsStatic',
		'elementsAngular',
		'img',
		'fonts'
	],
	callback);
});

// Process files and put into directory ready for production.
gulp.task('compile', function (callback) {
	runSequence(
	['build', 'clean:'+productionDir],
	[
	//	'compile:js',
		'compile:css',
		'compile:html',
		'compile:img',
		'compile:fonts'
	],
	callback);
});

// Run server.
gulp.task('server', ['build'], function (next) {
	var server = connect();
	server.use(connect.static('build')).listen(port, next);
});

// Watch task
gulp.task('watch:files', ['server'], function () {
//	gulp.watch('build.config.js', ['js:vendor']);

//	gulp.watch(files.js.app, ['js:app']);

	gulp.watch(files.html.tpls.all, ['js:templates-all']);

	gulp.watch(files.sass.main, ['css']);

	gulp.watch(files.html.index, ['html']);

	gulp.watch(files.img.src, ['img']);

	// Livereload
//	var server = livereload();

	gulp.watch('build/**/*', function (event) {
	server.changed(event.path);
	});
});

// Run unit & e2e tests and watch for changes.
//gulp.task('watch:test', ['karma:watch', 'protractor:watch']);

// Build, run server, run unit & e2e tests and watch for changes.
// gulp.task('watch', function (callback) {
// 	runSequence('watch:files', 'watch:test', callback);
// });

// Same as watch:files.
gulp.task('default', ['watch:files']);



/**
 * Generate tasks for Angular JS template caching
 *
 * @param {string} folder
 * @return stream
 */
// function gulpJSTemplates (folder) {
// 	gulp.task('js:templates-'+folder, function () {
// 	return gulp.src(files.html.tpls[folder])
// 		.pipe(html2js({
// 			outputModuleName: 'templates',
// 			useStrict: true,
// 			base: 'scr/'
// 		}))
// 		.pipe(concat('templates.js'))
// 		.pipe(gulp.dest(files.js.buildDest));
// 	});
// }

/**
 * Generate cleaning tasks.
 *
 * @param {string} folder
 * @return stream
 */
function gulpClean (folder) {
	gulp.task('clean:'+folder, function () {
	return gulp.src(folder, {read: false, force: true})
		.pipe(clean());
	});
}