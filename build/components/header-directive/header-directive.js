angular.module('app.components')

.directive( 'atHeader', function () {
	return {
		restrict: 'EA',
		replace: true,
		templateUrl: 'components/header-directive/header-directive-template.html',
		controller: function($scope, $window, $attrs) {

			$scope.menuHeight = {'height': 'auto'};
			$scope.showmenu = true;

			var initialWidth = $window.innerWidth,
				smallWidth   = 900,
				mobilemenu = false;

			var setSmall = function () {
				//$scope.menuHeight = {'height': '0px'};
				$scope.menuHeight = {'height': '1000px'};
				$scope.showmenu = false;
				$scope.$apply();
			};

			var setLarge = function () {
				$scope.menuHeight = {'height': 'auto'};
				$scope.showmenu = true;
				$scope.$apply();
			};

			if (initialWidth < smallWidth) {
				mobilemenu = true;
				setSmall();
			} else {
				mobilemenu = false;
				setLarge();
			}

			angular.element($window).on('resize', function () {
				if ($window.innerWidth <= smallWidth && mobilemenu === false) {
					mobilemenu = true;
					setSmall();
				} else if ($window.innerWidth > smallWidth && mobilemenu === true) {
					mobilemenu = false;
					setLarge();
				}
			});

			//this is the toggle function
			$scope.toggleMenu = function() {
				if ($window.innerWidth <= smallWidth) {
					$scope.showmenu = ($scope.showmenu) ? false : true;
					var newHeight = $('.active').closest('ul').height();
					$scope.menuHeight = ($scope.showmenu) ? {'height': ''+(newHeight + 102)+'px'} : {'height': '0px'};
				}
			};
		}
	};
});