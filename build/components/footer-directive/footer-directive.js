angular.module('app.components')

.directive( 'atFooter', function () {
	return {
		restrict: 'EA',
		replace: true,
		templateUrl: 'components/footer-directive/footer-directive-template.html'
	};
});